import { boot } from 'quasar/wrappers'

class ApiService {
  axios = void 0
  constructor (axios) {
    this.axios = axios
  }

  getProducts (data) {
    return this.axios.get('products', data)
  }

  addProduct (data) {
    return this.axios.post('products', data)
  }

  editProduct (data) {
    return this.axios.put(`products/${data.id}`, data)
  }

  archiveProduct (data) {
    return this.axios.put(`products/archive/${data.id}`, data)
  }

  deleteProduct (data) {
    return this.axios.delete(`products/${data.id}`, data)
  }
}

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app }) => {
  app.config.globalProperties.$api = new ApiService(
    app.config.globalProperties.$axios
  )
})
