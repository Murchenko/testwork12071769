<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $price = $this->faker->numberBetween(100000);
        $salesPrice = $this->faker->numberBetween(100000, $price);

        return [
            'title' => $this->faker->name,
            'excerpt' => $this->faker->realText(60),
            'description' => $this->faker->realText(),
            'image' => $this->faker->image('public/images/products', 640, 480, null, false),
            'price' => $price,
            'sales_price' => $salesPrice,
        ];
    }
}
