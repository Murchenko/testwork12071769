<?php


namespace App\Http\Services;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class ImageService
{

    const MAIN_FOLDER = '/images/';

    public static function save($file, $path)
    {

        $props = null;

        $imgName = '_' . uniqid() . time() . '.' . $file->extension();

        $img = Image::make($file->path());

        if ($props && isset($props->w) && isset($props->h) && isset($props->x) && isset($props->y)) {
            $img->crop($props->w, $props->h, $props->x, $props->y);
        }

        $destinationPath = public_path(self::MAIN_FOLDER . $path);

        (new self)->createDIR($destinationPath);

        $img->save($destinationPath . '/' . $imgName);

        return $imgName;
    }

    public function createDIR($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
    }
}
