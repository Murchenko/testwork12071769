<?php


namespace App\Http\Services;

use App\Models\Product;

class ProductService
{
    public function __construct()
    {
    }

    public function get($data)
    {
        $limit   = $data['limit'] ?? 15;
        $offset  = $data['offset'] ?? 0;

        $query = Product::limit($limit)->offset($offset);

        $products = $query->get();
        $count = Product::count();
        return ['products' => $products, 'count' =>  $count];
    }

    public function store($data)
    {
        $imagePath = ImageService::save($data['image'], 'products');
        $data['image'] = $imagePath;
        $newProduct = Product::create($data);
        return $newProduct;
    }

    public function update($data)
    {
        $imagePath = null;

        $product = Product::find($data['id']);

        if (isset($data['image'])) {
            $imagePath = ImageService::save($data['image'], 'products');
        }

        if ($imagePath) {
            $data['image'] = $imagePath;
        }


        $updateProduct = $product->update($data);
        return $updateProduct;
    }

    public function archive($data)
    {
        $product = Product::find($data['id']);
        $product->archive = '1';
        $product->save();

        return 1;
    }

    public function delete($data)
    {
        $product = Product::find($data['id']);
        $product->delete();

        return 1;
    }
}
