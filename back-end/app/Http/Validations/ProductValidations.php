<?php


namespace App\Http\Validations;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Validator as V;

class ProductValidations
{
    public function get(Request $request): V
    {
        return Validator::make($request->only('limit', 'offset'), [
            'limit' => 'integer',
            'offset' => 'integer',
        ]);
    }

    public function store(Request $request): V
    {
        return Validator::make($request->only('title', 'excerpt', 'description', 'image', 'price', 'sales_price'), [
            'title' => 'required|string',
            'excerpt' => 'string',
            'description' => 'string',
            'image' => 'required|image',
            'price' => 'required|integer',
            'sales_price' => 'integer',
        ]);
    }

    public function update(Request $request): V
    {
        $onlyNeedParams = $request->only('id', 'title', 'excerpt', 'description', 'image', 'price', 'sales_price');
        $onlyNeedParams['id'] = $request->id;

        return Validator::make($onlyNeedParams, [
            'id' => 'required|integer|exists:products,id',
            'title' => 'string',
            'excerpt' => 'string',
            'description' => 'string',
            'image' => 'image',
            'price' => 'integer',
            'sales_price' => 'integer',
        ]);
    }

    public function archive(Request $request): V
    {
        return Validator::make(['id' => $request->id], [
            'id' => 'required|integer|exists:products,id'
        ]);
    }

    public function delete(Request $request): V
    {
        return Validator::make(['id' => $request->id], [
            'id' => 'required|integer|exists:products,id'
        ]);
    }
}
