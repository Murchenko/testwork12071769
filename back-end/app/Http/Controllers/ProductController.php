<?php

namespace App\Http\Controllers;

use App\Http\Services\ProductService;
use App\Http\Validations\ProductValidations;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public $validator;

    public function __construct()
    {
        $this->validator = new ProductValidations;
        $this->productService = new ProductService;
    }

    public function get(Request $request)
    {
        $validData = $this->validation($request);

        return $this->productService->get($validData);
    }

    public function store(Request $request)
    {
        $validData = $this->validation($request);

        return $this->productService->store($validData);
    }

    public function update(Request $request)
    {
        $validData = $this->validation($request);

        return $this->productService->update($validData);
    }

    public function archive(Request $request)
    {
        $validData = $this->validation($request);

        return $this->productService->archive($validData);
    }

    public function delete(Request $request)
    {
        $validData = $this->validation($request);

        return $this->productService->delete($validData);
    }
}
