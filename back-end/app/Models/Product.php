<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Product extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['full_image'];

    public function getFullImageAttribute()
    {
        return URL::to('/') . '/images/products/' .  $this->image;
    }
}
